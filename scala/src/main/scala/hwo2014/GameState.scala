package hwo2014
import java.io.PrintWriter
import org.json4s.native.Serialization
import org.json4s._
import org.json4s.DefaultFormats


class GameState (writer: PrintWriter) {
  import hwo2014._
  implicit val formats = new DefaultFormats{}

  trait CarStatus
  case object Crashed extends CarStatus
  case object Running extends CarStatus

  trait SectionType
  case object Straight extends SectionType
  case object Angle extends SectionType
  
  var status: CarStatus = Running
  
  var carColor: String = ""
  var carName: String = ""
  var currentGame = ""
  var currentTick: Int = 0
  var lastPieceId: Int = 0
  var lastPosition: Double = 0
  var lastSpeed: Double = 0
  
  var track: Option[RaceTrack] = None

  var currentPieceType: Option[SectionType] = None
  var middleDistanceToCorner: Double = 0  
  
  def setYourCar(car: CarId, gameId: String) {
    carColor = car.color
    carName = car.name
    currentGame = gameId
    println("My color: " + carColor)
    println("My name: " + carName)
  }
  
  def setGameInit(gameInit: GameInitData) {
    println("game init")
    println(gameInit)
    track = Some(new RaceTrack(gameInit.race.track.pieces, gameInit.race.track.lanes))
  }
  
  def start() {
    println("start")
    throttle(1.0)
  }
  
  def setCarPositions(data: List[CarPosition], gameId: String, gameTick: Int) {
  
    def getMyCarPosition(): PiecePosition = {
      data.find(x => x.id.color == carColor).get.piecePosition
    }
    
    currentTick = gameTick
//    println("Tick: " + currentTick)
    //if (status == Crashed) return
    
    
    
    val myCarPosition: PiecePosition = getMyCarPosition()
    track match {
      case None => throttle(1.0)
      case Some(track) => {
        val speed= track.getDistance(lastPieceId, lastPosition, myCarPosition.pieceIndex, myCarPosition.inPieceDistance, myCarPosition.lane.startLaneIndex)
        if (speed < 0) {
          println("Negative speed: " + speed, lastPieceId, lastPosition, myCarPosition.pieceIndex, myCarPosition.inPieceDistance)
          throw new DontCatchMe("Negative speed")
        }
        lastPieceId = myCarPosition.pieceIndex
        lastPosition = myCarPosition.inPieceDistance
        if (speed != 0) {
          println(s"speed: $speed, acceleration: " + (speed - lastSpeed))
          lastSpeed = speed
        }
        val piece = track.pieces(lastPieceId)
        //val middle = piece.length / 2

        val distanceToNextCorner: Double = track.getDistanceToNextCorner(lastPieceId, lastPosition)
        
        var newPieceType = if (piece.angle != 0) Angle else Straight
        currentPieceType match {
          case None => {
            newPieceType match {
              case Angle => throttle(0.6) 
              case Straight => {
                middleDistanceToCorner = distanceToNextCorner * 0.2
                throttle(1.0)
              }
            }
          }
          case Some(Straight) =>
            newPieceType match {
              case Angle => throttle(0.5) 
              case Straight => {
                println(s"continue straight $distanceToNextCorner > $middleDistanceToCorner")
                if (distanceToNextCorner > middleDistanceToCorner || speed <= 6) {
                  println("full throttle")
                  throttle(1.0)
                } else if (speed > 6.5) {
                  throttle(0.0)
                } else{
                  throttle(0.5)
                }
              }
            }
          
          case Some(Angle) => 
            newPieceType match {
              case Angle => 
                if (speed < 5.5) throttle(1.0)
                else if (speed < 6.0) throttle(0.7)
                else throttle(0.5) 
              case Straight => {
                println("start straight")
                middleDistanceToCorner = distanceToNextCorner * 0.2
                throttle(1.0)
              }
           }
        }
        currentPieceType = Some(newPieceType)
        /*
        if (speed < 4.5) {
          throttle(1.0)
        } else if (piece.radius > 0) {
          if (lastPosition < middle) {
            if (speed > 5.0) {
              throttle(0.0)
            } else  {
              throttle(0.5)
            }
          } else {
            throttle(0.7)
          }
        } else if (lastPosition < middle) {
          throttle(1.0)
        } else if (speed > 6.0) {
          throttle(0.0)
        } else{
          throttle(0.5)
        }
        */
      }
    }
  }
  
  private def calculateStraightDetails(currentPieceId: Int, distanceInPiece: Double) {
    
  }
  
  def crash(data: CarId, gameId: String, gameTick: Int) {
    println("crashed")
    status = Crashed
  }
  
  def spawn(data: CarId, gameId: String, gameTick: Int) {
    println("respawned")
    status = Running
  }
  
  private def throttle(value: Double) {
    send(MsgWrapper("throttle", value))
  }
  
  private def ping {
    send(MsgWrapper("ping", 0))
  }
  
  def send(msg: MsgWrapper) {
    println("Send: " + msg)

    writer.println(Serialization.write(msg))
    writer.flush
  }


}