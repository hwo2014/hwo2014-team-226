package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}


case class MsgWrapper(msgType: String, data: JValue)

object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  val gameState = new GameState(writer)
  
  
  
  gameState.send(MsgWrapper("join", JoinData(botName, botKey)))
  play

  @tailrec 
  private def play {
    implicit val formats = new Formats {
      val dateFormat = DefaultFormats.lossless.dateFormat
      override val typeHints = ShortTypeHints(classOf[join] :: classOf[yourCar] :: classOf[gameInit] :: classOf[gameStart] :: classOf[carPositions] :: classOf[gameEnd] :: classOf[tournamentEnd] :: classOf[crash] :: classOf[spawn] :: classOf[lapFinished] :: classOf[dnf] :: classOf[finish] :: Nil)
      override val typeHintFieldName = "msgType"
    }
    
    val line = reader.readLine()
    if (line != null) {
      try {
        val command = Serialization.read[GameCommand](line)
        command match {
          case yourCar(data, gameId) => gameState.setYourCar(data, gameId)
          case gameInit(data) => gameState.setGameInit(data)
          case gameStart(data) => gameState.start
          case carPositions(cars, gameId, tick) => gameState.setCarPositions(cars, gameId, tick)
          case crash(data, gameId, gameTick) => gameState.crash(data, gameId, gameTick)
          case spawn(data, gameId, gameTick) => gameState.spawn(data, gameId, gameTick)
          case _ => 
        }
      } catch {
        case e: DontCatchMe => throw e
        case e: Throwable => {
          println(line)
          println(e)
        }
      }
      play
    }
  }
    /*
  private def handle(command: Command.GameCommand) {
    command match {
      case join(data) =>
      /*case yourCar(carId) =>
      case gameInit(initData) =>
      case gameStartCommand =>
      case carPositions(data, gameId, gameTick) => println("Position: " + data)
      case crash(data, gameId, gameTick) =>
      case spawn(data, gameId, gameTick) =>
      case gameEnd(data) =>
      case tournamentEndCommand => */
      case _ =>
    }
  }
  * 
  */

}

