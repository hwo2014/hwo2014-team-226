package hwo2014

class RaceTrack(rawPieces: List[Piece], lanes: List[Lane]) {
  
  def lengthOfArc(radius: Double, angle: Double): Double = math.Pi * radius * math.abs(angle) / 180
  
  val pieces = rawPieces.map(_ match {
    case piece if (piece.angle != 0) => {
      println(s"Piece: $piece, length: " + lanes.map(lane => lane.index -> lengthOfArc(piece.radius - (lane.distanceFromCenter * math.signum(piece.angle)), piece.angle)).toMap)
      Piece(radius = piece.radius, angle = piece.angle, lengthByLane = lanes.map(lane => lane.index -> lengthOfArc(piece.radius - (lane.distanceFromCenter * math.signum(piece.angle)), piece.angle)).toMap)
    }
    case piece if (piece.length == 0.0) => {
      println("Zero length piece: " + piece)
      throw new DontCatchMe("Zero length piece: " + piece)
    }
    case piece => {
      Piece(length = piece.length, switch = piece.switch, lengthByLane = lanes.map(lane => lane.index -> piece.length).toMap)
    }
  })
  
  val twoLaps: List[Piece] = pieces ++ pieces
  
  val sections: List[Piece] = concatPiecesToSections(pieces) 
  
  val laneCount = lanes.size
  val pieceCount = pieces.size
  
  //4,84.76856620537329,5,4.07939690754651
  def getDistance(startId: Int, startPosition: Double, endId: Int, endPosition: Double, lane: Int): Double = {
    if (endId == startId) {
      println(s"Position: $endPosition, lane: $lane")
      return endPosition - startPosition
    } else {
      println("Length of previous piece: " + pieces(startId).lengthByLane(lane))
      println("Length of this piece: " + pieces(endId).lengthByLane(lane))
      return endPosition + pieces(startId).lengthByLane(lane) - startPosition
    }
  }
  
  def getDistanceToNextCorner(currentPieceId: Int, distanceInPiece: Double): Double = {
    val currentPiece = pieces(currentPieceId)
    if (currentPiece.angle != 0) 0.0
    else {
      val piecesTillNextCorner = twoLaps.drop(currentPieceId).takeWhile(p => p.angle == 0)
      piecesTillNextCorner.map(p => p.length).sum - distanceInPiece
    }
  }
  
  private def concatPiecesToSections(pieces: List[Piece]): List[Piece] = {
    pieces
  }
}
