package hwo2014 {

  trait GameCommand
  
  //commands
  case class join(data: JoinData) extends GameCommand
  case class yourCar(data: CarId, gameId: String) extends GameCommand
  case class gameInit(data: GameInitData) extends GameCommand
  case class gameStart(data: String) extends GameCommand
  case class carPositions(data: List[CarPosition], gameId: String, gameTick: Int = 0) extends GameCommand
  case class gameEnd(data: GameEndData) extends GameCommand
  case class tournamentEnd extends GameCommand
  case class crash(data: CarId, gameId: String, gameTick: Int) extends GameCommand
  case class spawn(data: CarId, gameId: String, gameTick: Int) extends GameCommand
  case class lapFinished(data: LapFinishedData, gameId: String, gameTick: Int = 0) extends GameCommand
  case class dnf(data: DnfData, gameId: String, gameTick: Int) extends GameCommand
  case class finish(data: CarId, gameId: String, gameTick: Int = 0) extends GameCommand
  case class ping extends GameCommand
  
  //data classes
  case class JoinData(name: String, key: String)
  case class GameInitData(race: Race)
  
  /*
  trait Piece
  case class StraightPiece(length: Double) extends Piece
  case class SwitchPiece(length: Double, switch: Boolean) extends Piece
  case class RoundedPiece(radius: Double, angle: Double) extends Piece
  */
  case class Piece(length: Double = 0, switch: Boolean = false, radius: Double = 0, angle: Double = 0, lengthByLane: Map[Int,Double] = Map.empty)

  case class Lane(distanceFromCenter: Int, index: Int)
  case class Position(x: Double, y: Double)
  case class StartingPoint(position: Position, angle: Double)
  
  case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint)
  
  case class CarId(name: String, color: String)
  case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
  
  case class Car(id: CarId, dimensions: CarDimensions)
  case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)
  
  case class Race(track: Track, cars: List[Car], raceSession: RaceSession)
  
  case class PieceLane(startLaneIndex: Int, endLaneIndex: Int)
  case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: PieceLane, lap: Int)
  case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)
  
  case class Result(laps: Int, ticks: Int, millis: Int)
  case class CarResult(car: CarId, result: Result)
  case class CarLapResult(car: CarId, result: LapTime)
  case class GameEndData(results: List[CarResult], bestLaps: List[CarLapResult])
  
  case class LapFinishedData(car: CarId, lapTime: LapTime, raceTime: Result, ranking: Ranking)
  case class Ranking(overall: Int, fastestLap: Int)
  case class LapTime(lap: Int, ticks: Int, millis: Int)
  
  case class DnfData(car: CarId, reason: String)
}